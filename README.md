# almundo.com Frontend Test

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

```
MongoDB  (https://docs.mongodb.com/manual/installation/)
NodeJS   (https://nodejs.org/es/download/)
Yarn     (https://yarnpkg.com/lang/en/docs/install/)
```

### Installing

Global dependencies:
```bash
yarn global add @angular/cli
```
**In the project folder:**

Seed data:
```bash
#in project folder
mongoimport --db almundo --collection hotels --drop --file data/mongo-data-import.json 
```

Create .env file and add this:
```
DB_URL=localhost
DB_NAME=almundo
PORT=3000
```

Install dependencies:
```bash
yarn
```

Run Development:
```bash
yarn server
#in another shell tab
yarn start

go to http://localhost:4200/
```

Run Production:
```bash
#add NODE_ENV=production in .env
yarn buildandstartprod

go to http://localhost:3000/
```


All Commands:

commands | function
---       | ---
`yarn start` | Start frontend with autoload (ng serve)
`yarn build` | Build frontend in `./dist` folder
`yarn server` | Start express app.
`yarn build` | Build frontend in `./dist` folder
`yarn buildandstart` | `yarn build` && `yarn server`
`yarn buildandstartprod` | Build and run server(frontend in production)

End with an example of getting some data out of the system or using it for a little demo

### Coding style
The project uses [Prettier](https://prettier.io/).


## Built With
####Backend:
[MongoDB](https://www.mongodb.com/es) 
[Mongoose](http://mongoosejs.com/) 
[Dotenv](https://github.com/motdotla/dotenv) 
[ExpressJS 4](http://expressjs.com/es/) 

####Frontend:
[Angular 5](https://angular.io/) 
[Angular Bootstrap 4](https://valor-software.com/ngx-bootstrap/#/) 


## Authors

* **Emiliano Fernandez** 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
