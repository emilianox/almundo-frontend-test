var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Hotel = require("../models/hotel.js");

router.get("/", function(req, res, next) {
  Hotel.find(function(err, hotels) {
    if (err) return next(err);
    res.json(hotels);
  });
});



router.get("/q", function(req, res, next) {
  // TO TEST:
  // const query = {
  //   $text: { $search: "hotel" },
  //   amenities: { $all: ["garden", "nightclub"] },
  //   stars: { $in: [1, 2,4,5] }
  // };
  const query = {}

  if (req.query.name) {
    query.$text = {$search:req.query.name}
  }
  if (req.query.stars) {
    query.stars = { $in: req.query.stars };
  }

  Hotel.find(query, function(
    err,
    hotels
  ) {
    if (err) return next(err);
    res.json(hotels);
  });
});


/* save */
router.post('/', function(req, res, next) {
  Hotel.create(req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* update */
router.put('/:id', function(req, res, next) {
  Hotel.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* update*/
router.delete('/:id', function(req, res, next) {
  Hotel.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});




module.exports = router;
