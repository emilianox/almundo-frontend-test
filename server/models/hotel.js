var mongoose = require("mongoose");



var HotelSchema = new mongoose.Schema({
  id: String,
  name: { type: String, index: true },
  stars: Number,
  price: Number,
  image: String,
  amenities: [String]
});


HotelSchema.index({
  name: "text"
});


module.exports = mongoose.model("Hotel", HotelSchema);
