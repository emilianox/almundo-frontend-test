import { IHotel } from '../../services/hotel.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: "app-hotel-info",
  templateUrl: "./hotel-info.component.html",
  styleUrls: ["./hotel-info.component.css"]
})
export class HotelInfoComponent implements OnInit {
  @Input() hotel: IHotel;

  constructor() {}

  ngOnInit() {}

  /**
   * getStarArray
   */
  public getStarArray(i:number) {
    return Array(i);
  }
}
