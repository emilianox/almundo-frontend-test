import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: "app-stars-filter",
  templateUrl: "./stars-filter.component.html",
  styleUrls: ["./stars-filter.component.css"]
})
export class StarsFilterComponent implements OnInit {
  @Input() search: (starsToSearch) => void;
  allStars = [5, 4, 3, 2, 1];
  selectedStars = {
    all: true,
    1: false,
    2: false,
    3: false,
    4: false,
    5: false
  };

  constructor() {}

  onChangeStar(keyChanged: string | number) {
    let sendStars = [];
    if (keyChanged === "all") {
      this.selectedStars = {
        all: !this.selectedStars[keyChanged],
        1: false,
        2: false,
        3: false,
        4: false,
        5: false
      };
      if (this.selectedStars["all"]) {
        sendStars = [];
      }
    } else {
      this.selectedStars.all = false;
      this.selectedStars[keyChanged] = !this.selectedStars[keyChanged];
      sendStars = Object.keys(this.selectedStars).filter(
        key => this.selectedStars[key]
      );
      console.log("sendStars", sendStars);
    }

    this.search(sendStars);
  }

  isCollapsed: boolean = false;

  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  ngOnInit() {}
}
