import { Component, HostListener, Input, OnInit } from '@angular/core';

@Component({
  selector: "app-hotel-search-sidebar",
  templateUrl: "./hotel-search-sidebar.component.html",
  styleUrls: ["./hotel-search-sidebar.component.css"]
})
export class HotelSearchSidebarComponent implements OnInit {
  @Input() search: (textSearch, starsSearch) => void;
  starsSearch: number[] = [];
  textSearch: string = "";
  isCollapsed: boolean = false;
  isMobile: boolean = true;

  constructor() {
    if (window.screen.width >= 768) {
      this.isMobile = false
    }
  }

  ngOnInit() {}

  public searchByName = newTextToSearch => {
    this.textSearch = newTextToSearch;
    this.search(this.textSearch, this.starsSearch);
  };

  public searchByStars = newStarsToSearch => {
    this.starsSearch = newStarsToSearch;
  };


  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.isMobile = (window.screen.width >= 768) ? false : true
    this.isCollapsed = (window.screen.width >= 768) ? false : true
  }
}

