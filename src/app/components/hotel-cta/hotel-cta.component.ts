import { Component, Input, OnInit } from '@angular/core';
import { IHotel } from '../../services/hotel.service';

@Component({
  selector: "app-hotel-cta",
  templateUrl: "./hotel-cta.component.html",
  styleUrls: ["./hotel-cta.component.css"]
})
export class HotelCtaComponent implements OnInit {
  @Input() hotel: IHotel;

  constructor() {}

  ngOnInit() {}
}
