import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelCtaComponent } from './hotel-cta.component';

describe('HotelCtaComponent', () => {
  let component: HotelCtaComponent;
  let fixture: ComponentFixture<HotelCtaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelCtaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelCtaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
