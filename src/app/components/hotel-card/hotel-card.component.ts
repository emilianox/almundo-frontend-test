import { IHotel } from '../../services/hotel.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: "app-hotel-card",
  templateUrl: "./hotel-card.component.html",
  styleUrls: ["./hotel-card.component.css"]
})
export class HotelCardComponent implements OnInit {
  @Input() hotel: IHotel;

  constructor() {
    // this.hotel = { id: "249942", name: "Hotel Stefanos", stars: 3, price: 994.18, image: "4900059_30_b.jpg", amenities: ["safety-box", "nightclub", "deep-soaking-bathtub", "beach", "business-center"] };
  }

  ngOnInit() {}
}
