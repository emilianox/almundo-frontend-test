import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: "app-name-filter",
  templateUrl: "./name-filter.component.html",
  styleUrls: ["./name-filter.component.css"]
})
export class NameFilterComponent implements OnInit {
  @Input() search: (nameToSearch) => void;
  constructor() {}

  ngOnInit() {}

  isCollapsed: boolean = false;

  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }
}
