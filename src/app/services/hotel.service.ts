

import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { catchError, map, tap } from "rxjs/operators";

import { environment } from "./../../environments/environment";


const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

export interface IHotel {
  id: string;
  name: string;
  stars: number;
  price: number;
  image: string;
  amenities: string[];
}

@Injectable()
export class HotelService {
  private hotelsUrl = `${environment.baseUrl}/hotels`; // URL to web api

  constructor(private http: HttpClient) {}

  /** GET hotels from the server */
  getHotels(): Observable<IHotel[]> {
    return this.http
      .get<IHotel[]>(this.hotelsUrl)
      .pipe(
        tap(hotels => this.log(`fetched hotels`)),
        catchError(this.handleError("getHotels", []))
      );
  }


  /** GET hotel by id. Will 404 if id not found */
  getHotel(id: number): Observable<IHotel> {
    const url = `${this.hotelsUrl}/${id}`;
    return this.http
      .get<IHotel>(url)
      .pipe(
        tap(_ => this.log(`fetched hotel id=${id}`)),
        catchError(this.handleError<IHotel>(`getHotel id=${id}`))
      );
  }

  /* GET hotels whose name contains search term */
  searchHotels(name: string, stars:string[]): Observable<IHotel[]> {
    // if (!name.trim()) {
    //   // if not search term, return empty hotel array.
    //   return of([]);
    // }

    let httpParams = {stars, name}

    return this.http
      .get<IHotel[]>(`${this.hotelsUrl}/q?`,{params:httpParams})
      .pipe(
        tap(_ => this.log(`found hotels matching "${name}"`)),
        catchError(this.handleError<IHotel[]>("searchHotels", []))
      );
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HotelService message with the MessageService */
  private log(message: string) {
    console.log(message)
  }
}
