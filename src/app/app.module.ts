import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { RatingModule, CollapseModule } from "ngx-bootstrap";

import { AppComponent } from './app.component';
import { HotelCardComponent } from './components/hotel-card/hotel-card.component';
import { HotelCtaComponent } from './components/hotel-cta/hotel-cta.component';
import { HotelInfoComponent } from './components/hotel-info/hotel-info.component';
import { HotelSearchSidebarComponent } from './components/hotel-search-sidebar/hotel-search-sidebar.component';
import { NameFilterComponent } from './components/name-filter/name-filter.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { StarsFilterComponent } from './components/stars-filter/stars-filter.component';
import { HotelSearchPageComponent } from './pages/hotel-search-page/hotel-search-page.component';
import { HotelService } from './services/hotel.service';
import { environment } from "./../environments/environment";


const appRoutes: Routes = [
  {
    path: '',
    component: HotelSearchPageComponent,
    data: { title: 'Hotel Search' }
  }
];


@NgModule({
  declarations: [
    AppComponent,
    NameFilterComponent,
    StarsFilterComponent,
    HotelCardComponent,
    HotelInfoComponent,
    HotelCtaComponent,
    NavbarComponent,
    HotelSearchPageComponent,
    HotelSearchSidebarComponent
  ],
  imports: [
    RatingModule.forRoot(),
    CollapseModule.forRoot(),
    HttpClientModule,
    FormsModule,

    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: !environment.production } // <-- debugging purposes only
    )
  ],
  providers: [HotelService],
  bootstrap: [AppComponent]
})
export class AppModule {}
