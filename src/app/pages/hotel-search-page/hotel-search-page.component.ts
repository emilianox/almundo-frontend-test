import { HotelService, IHotel } from '../../services/hotel.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: "app-hotel-search-page",
  templateUrl: "./hotel-search-page.component.html",
  styleUrls: ["./hotel-search-page.component.css"]
})
export class HotelSearchPageComponent implements OnInit {
  hotels: IHotel[];

  constructor(private hotelService: HotelService) {}

  ngOnInit() {

    this.subscribeToHotels();
  }

  subscribeToHotels(): void {
    this.hotelService.getHotels().subscribe(hotels => (this.hotels = hotels));
  }


  search = (name, stars) =>{
    this.hotelService
      .searchHotels(name, stars)
      .subscribe(hotels => (this.hotels = hotels));
  }
}
