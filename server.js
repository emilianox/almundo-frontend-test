// Get dependencies
require("dotenv").config();
const express = require("express");
const path = require("path");
const http = require("http");
const bodyParser = require("body-parser");
var cors = require("cors");

var mongoose = require("mongoose");
mongoose.Promise = global.Promise;

mongoose
.connect(`mongodb://${process.env.DB_URL}/${process.env.DB_NAME}`, {
  useMongoClient: true
})
.then(() => console.log("connection with mongodb succesful"))
.catch(err => console.error(err));



// Get our API routes
const hotels = require("./server/routes/hotels");
const app = express();

if (app.get("env") === "development") {
  app.use(cors());
  mongoose.set("debug", true);
}

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Point static path to dist
app.use(express.static(path.join(__dirname, "dist")));

// Set our api routes
app.use("/hotels", hotels);

// Catch all other routes and return the index file
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "dist/index.html"));
});

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || "3000";
app.set("port", port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));
